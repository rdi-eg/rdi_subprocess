include(${cmake_scripts_path}/rdi_external_project_add.cmake)

rdi_external_project_add(rdi_wfile
	GIT_REPO   "git@arwash.rdi:common/wfile.git"
	PATH       ${workspace_location}/common/wfile/
)

add_executable(test
	main.cpp
	tests.cpp
)

find_package(OpenMP REQUIRED)

target_link_libraries(test PRIVATE rdi_subprocess rdi_wfile OpenMP::OpenMP_CXX)

target_compile_definitions(test PRIVATE
	"-DCODE_LOCATION=\"${CMAKE_SOURCE_DIR}/test/\""
)

if(WIN32)
	include(${cmake_scripts_path}/rdi_prebuilt.cmake)
	rdi_prebuilt_libraries(test openmp ${workspace_location})
endif()

include(${cmake_scripts_path}/rdi_download_unittest_header.cmake)
download_catch(${CMAKE_CURRENT_SOURCE_DIR})
