﻿#include <time.h>
#include <tchar.h>
#include <stdio.h>
#include <assert.h>
#include <windows.h>
#include <strsafe.h>

#include "rdi_subprocess.hpp"

#define UNUSED(x) (void)(x)

struct Stream
{
	RDI_Buffer b;
	size_t read_bytes;
	DWORD last_read;
	OVERLAPPED overlapped;
};

typedef struct Stream Stream;

static Stream
stream_init()
{
	Stream s;
	s.b = rdi_buffer_init();
	s.read_bytes = 0;
	s.last_read = 0;
	memset(&s.overlapped, 0, sizeof(OVERLAPPED));
	return s;
}

static volatile long pipe_serial_number = 0;

/// Stolen from this SO answer: https://stackoverflow.com/a/419736/3659011
/// @brief Creates a pipe that can be used asynchronously.
///
/// @param read_side - Returns a handle to the read side of the pipe. Data
/// 		may be read from the pipe by specifying this handle value in a
/// 		subsequent call to ReadFile.
/// @param write_side - Returns a handle to the write side of the pipe. Data
/// 		may be written to the pipe by specifying this handle value in a
/// 		subsequent call to WriteFile.
/// @param pipe_attributes - An optional parameter that may be used to specify
/// 		the attributes of the new pipe. If the parameter is not
/// 		specified, then the pipe is created without a security
/// 		descriptor, and the resulting handles are not inherited on
/// 		process creation. Otherwise, the optional security attributes
/// 		are used on the pipe, and the inherit handles flag effects both
/// 		pipe handles.
/// @param suggested_size - Supplies the requested buffer size for the pipe.
///			This is only a suggestion and is used by the operating system to
/// 		calculate an appropriate buffering mechanism. A value of zero
/// 		indicates that the system is to choose the default buffering
/// 		scheme.
static void
anonymous_pipe_create(
	OUT LPHANDLE read_side,
	OUT LPHANDLE write_side,
	IN LPSECURITY_ATTRIBUTES pipe_attributes,
	IN DWORD suggested_size,
	IN DWORD read_flags,
	IN DWORD write_flags)
{
	if ((read_flags | write_flags) & (~FILE_FLAG_OVERLAPPED))
	{
		fprintf(stderr, "Invalid flags passed to async_pipe_create\n");
		exit(1);
	}

	char pipe_name[MAX_PATH];

	if (suggested_size == 0)
	{
		suggested_size = 4096;
	}

	StringCbPrintfA(pipe_name,
					MAX_PATH,
					"\\\\.\\Pipe\\%08x.%08x",
					GetCurrentProcessId(),
					InterlockedIncrement(&pipe_serial_number)
	);

	*read_side = CreateNamedPipeA(
		pipe_name,
		PIPE_ACCESS_INBOUND  | read_flags,
		PIPE_TYPE_BYTE,
		1,                                // Number of pipes
		suggested_size,                   // Out buffer size
		suggested_size,                   // In buffer size
		NMPWAIT_USE_DEFAULT_WAIT,         // Timeout in ms
		pipe_attributes
	);

	assert(*read_side != INVALID_HANDLE_VALUE);

	*write_side = CreateFileA(
		pipe_name,
		GENERIC_WRITE,
		0,
		pipe_attributes,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL | write_flags,
		NULL
	);

	assert(*write_side != INVALID_HANDLE_VALUE);
}

/// Will close the handles when it's finished
/// @returns handle to the newly created process
static HANDLE
child_process_create(const char* command,
					 HANDLE child_stdin,
					 HANDLE child_stdout)
{
	//by converting the command to wchar_t we can handle arabic input in the command

	int size_needed = MultiByteToWideChar( CP_UTF8 , 0 , command , -1, NULL , 0 );
	wchar_t* wcmd = malloc(size_needed*sizeof(wcmd));
	MultiByteToWideChar(CP_UTF8, 0, command, -1, wcmd,size_needed);

	HANDLE original_child_stdin = child_stdin;
	HANDLE original_child_stdout = child_stdout;

	if (!child_stdin)
	{
		child_stdin = GetStdHandle(STD_INPUT_HANDLE);
	}

	if (!child_stdout)
	{
		child_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
	}

	PROCESS_INFORMATION process_info = { 0 };
	STARTUPINFO startup_info = { 0 };

	startup_info.cb = sizeof(STARTUPINFO);
	startup_info.hStdError = GetStdHandle(STD_ERROR_HANDLE);
	startup_info.hStdOutput = child_stdout;
	startup_info.hStdInput = child_stdin;
	startup_info.dwFlags |= STARTF_USESTDHANDLES;

	// The docs says that this function will modify the content of wcommand
	// https://msdn.microsoft.com/en-us/windows/ms682425(v=vs.90)
	// Not sure if that's a useful piece of information but I thought I
	// mention that just in case ¯\_(ツ)_/¯
	bool success = CreateProcessW(NULL,
		wcmd,  // command line
		NULL,          // process security attributes 
		NULL,          // primary thread security attributes 
		TRUE,          // handles are inherited 
		0,             // creation flags 
		NULL,          // use parent's environment 
		NULL,          // use parent's current directory 
		&startup_info,  // STARTUPINFO pointer 
		&process_info);  // receives PROCESS_INFORMATION 

	if (!success)
	{
		if(GetLastError() == ERROR_FILE_NOT_FOUND)
		{
			free(wcmd);
			return nullptr;
		}

		fprintf(stderr, "CreateProcess failed with error: %lu", GetLastError());
		free(wcmd);
		exit(1);
	}

	CloseHandle(process_info.hThread);

	CloseHandle(original_child_stdin);
	CloseHandle(original_child_stdout);
	free(wcmd);

	return process_info.hProcess;
}

/// Will close the handles when it's finished
static void
pipe_write(HANDLE pipe, RDI_Const_Buffer input)
{
	DWORD written_size;

	if (!WriteFile(pipe, input.data, (DWORD)input.size, &written_size, nullptr))
	{
		fprintf(stderr, "writing to pipe failed\n");
		exit(1);
	}

	assert(written_size == (DWORD)input.size);

	// Close the pipe handle so the child process stops reading. 
	if (!CloseHandle(pipe))
	{
		fprintf(stderr, "close pipe failed\n");
		exit(1);
	}
}

static Stream
pipe_read(HANDLE pipe, Stream out, size_t max_read_size)
{
	size_t max_array_size = INT_MAX - 1;

	if (out.read_bytes + max_read_size >= max_array_size)
	{
		// If resize would fail, don't read more, return what we have.
		return out;
	}
	
	out.b = rdi_buffer_resize(out.b, out.read_bytes + max_read_size);

	bool read_success = ReadFile(pipe, out.b.data + out.read_bytes,
		(DWORD)max_read_size, &out.last_read, nullptr);

	UNUSED(read_success);

	if (out.last_read > 0)
	{
		out.read_bytes += (size_t)out.last_read;
	}

	if (out.read_bytes > 0)
	{
		out.b = rdi_buffer_resize(out.b, out.read_bytes);
	}
	else if (out.read_bytes == 0)
	{
		rdi_buffer_free(out.b);
		out.b = rdi_buffer_init();
	}

	return out;
}

/// Will close the handles when it's finished
/// Read output from the child process's pipe for STDOUT
/// and write to the parent process's pipe for STDOUT.
/// Stop when there is no more data.
static RDI_Buffer
pipe_read_all(HANDLE pipe)
{
	const size_t CHUNK_SIZE = 4096;
	Stream output = stream_init();
	output.b = rdi_buffer_reserve(output.b, CHUNK_SIZE);
	do
	{
		output = pipe_read(pipe, output, CHUNK_SIZE);
		if ( output.b.capacity - output.b.size < CHUNK_SIZE )
		{
			output.b = rdi_buffer_reserve(output.b, output.b.size * 2);
		}
	} while (output.last_read > 0);

	if (output.read_bytes > 0)
	{
		output.b = rdi_buffer_resize(output.b, output.read_bytes);
	}

	if (!CloseHandle(pipe))
	{
		fprintf(stderr, "close pipe failed\n");
		exit(1);
	}

	return output.b;
}

RDI_Buffer
rdi_subprocess_o_s(int* status, const char* cmd_line)
{
	SECURITY_ATTRIBUTES security_attributes;

	security_attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
	security_attributes.bInheritHandle = true;
	security_attributes.lpSecurityDescriptor = nullptr;

	HANDLE sub_to_main[2];
	CreatePipe(&sub_to_main[1], &sub_to_main[0], &security_attributes, 0);

	if (!SetHandleInformation(sub_to_main[1], HANDLE_FLAG_INHERIT, 0))
	{
		fprintf(stderr, "SetHandleInformation failed\n");
		exit(1);
	}

	HANDLE child = child_process_create(cmd_line, nullptr, sub_to_main[0]);

	if(!child)
	{
		*status = COMMAND_DOES_NOT_EXIST;
		CloseHandle(sub_to_main[0]);
		CloseHandle(sub_to_main[1]);
		return rdi_buffer_init();
	}

	RDI_Buffer output = pipe_read_all(sub_to_main[1]);

	WaitForSingleObject(child, INFINITE);
	GetExitCodeProcess(child, (LPDWORD)status);
	CloseHandle(child);

	return output;
}

RDI_Buffer
rdi_subprocess_o_l(int* status, const char* argv[])
{
	size_t cmd_size = strlen(argv[0]) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, argv[0]);

	for(size_t i = 1;; i++)
	{
		const char* tmp = argv[i];

		if(tmp == nullptr)
		{
			break;
		}

		size_t tmp_size = strlen(tmp);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + tmp_size + 1);
		memcpy(cmd_line.data + old_size, tmp, tmp_size);
	}

	cmd_line.data[cmd_line.size - 1] = '\0';

	RDI_Buffer output = rdi_subprocess_o_s(status, cmd_line.data);

	rdi_buffer_free(cmd_line);

	return output;
}

// you need to free the returned char*
char* handle_spaces(char* str)
{
	// 3 = 1 null terminator + 2 double qoute characters
   size_t new_length = strlen(str) + 3;
   char* new_str = malloc(new_length * sizeof(char));
   // surround with double quotes
   // snprintf null terminates the string no worries
   snprintf(new_str, new_length, "\"%s\"", str);
   return new_str;
}

RDI_Buffer
rdi_subprocess_o(int* status, const char* command, ...)
{
	va_list ap;
	size_t n;

	va_start(ap, command);
	for (n = 1; va_arg(ap, char*); n++);
	va_end(ap);

	size_t cmd_size = strlen(command) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, command);

	va_start(ap, command);
	for (size_t i = 1; i < n; i++)
	{
		char* tmp = va_arg(ap, char*);

		char* arg = handle_spaces(tmp);
		size_t arg_size = strlen(arg);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + arg_size + 1);
		memcpy(cmd_line.data + old_size, arg, arg_size);
		free(arg);
	}
	va_end(ap);

	cmd_line.data[cmd_line.size - 1] = '\0';

	RDI_Buffer output = rdi_subprocess_o_s(status, cmd_line.data);

	rdi_buffer_free(cmd_line);

	return output;
}

void
rdi_subprocess_i_s(int* status, RDI_Const_Buffer input, const char* cmd_line)
{
	SECURITY_ATTRIBUTES security_attributes;

	security_attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
	security_attributes.bInheritHandle = true;
	security_attributes.lpSecurityDescriptor = nullptr;

	HANDLE main_to_sub[2];
	CreatePipe(&main_to_sub[1], &main_to_sub[0],
			   &security_attributes, (DWORD)input.size);

	if (!SetHandleInformation(main_to_sub[0], HANDLE_FLAG_INHERIT, 0))
	{
		fprintf(stderr, "SetHandleInformation failed\n");
		exit(1);
	}

	HANDLE child = child_process_create(cmd_line, main_to_sub[1], nullptr);

	if(!child)
	{
		*status = COMMAND_DOES_NOT_EXIST;
		CloseHandle(main_to_sub[0]);
		CloseHandle(main_to_sub[1]);
		return;
	}

	pipe_write(main_to_sub[0], input);

	WaitForSingleObject(child, INFINITE);
	GetExitCodeProcess(child, (LPDWORD)status);
	CloseHandle(child);
}

void
rdi_subprocess_i_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[])
{
	size_t cmd_size = strlen(argv[0]) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, argv[0]);

	for(size_t i = 1;; i++)
	{
		const char* tmp = argv[i];

		if(tmp == nullptr)
		{
			break;
		}

		size_t tmp_size = strlen(tmp);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + tmp_size + 1);
		memcpy(cmd_line.data + old_size, tmp, tmp_size);
	}

	cmd_line.data[cmd_line.size - 1] = '\0';

	rdi_subprocess_i_s(status, input_buffer, cmd_line.data);

	rdi_buffer_free(cmd_line);
}

void
rdi_subprocess_i(int* status, RDI_Const_Buffer input, const char* command, ...)
{
	va_list ap;
	size_t n;

	va_start(ap, command);
	for (n = 1; va_arg(ap, char*); n++);
	va_end(ap);

	size_t cmd_size = strlen(command) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, command);

	va_start(ap, command);
	for (size_t i = 1; i < n; i++)
	{
		char* tmp = va_arg(ap, char*);

		char* arg = handle_spaces(tmp);
		size_t arg_size = strlen(arg);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + arg_size + 1);
		memcpy(cmd_line.data + old_size, arg, arg_size);
		free(arg);
	}
	va_end(ap);

	cmd_line.data[cmd_line.size - 1] = '\0';

	rdi_subprocess_i_s(status, input, cmd_line.data);

	rdi_buffer_free(cmd_line);
}

RDI_Buffer
rdi_subprocess_io_s(int* status, RDI_Const_Buffer input, const char* cmd_line)
{
	// used to randomize the anonymous pipe names in async_pipe_create()
	srand((unsigned int)time(nullptr));

	SECURITY_ATTRIBUTES security_attributes;

	// diagram stolen from this SO answer:
	// https://stackoverflow.com/a/47560929/3659011
	// 	    Us                                          Child
	// +------------------+                        +---------------+
	// |                  |                        |               |
	// |         main_to_sub[0]   =====>   main_to_sub[1]          |
	// |                  |                        |               |
	// |         sub_to_main[1]   <=====   sub_to_main[0]          |
	// |                  |                        |               |
	// +------------------+                        +---------------+

	// parent writes to 0 and child reads from 1
	// parent --> 0 ===== 1 <stdin> child
	HANDLE main_to_sub[2];

	// child writes to 0 and parent reads from 1
	// child <stdout> --> 0 ===== 1 parent
	HANDLE sub_to_main[2];

	// Set the bInheritHandle flag so pipe handles are inherited.
	security_attributes.nLength = sizeof(SECURITY_ATTRIBUTES);
	security_attributes.bInheritHandle = true;
	security_attributes.lpSecurityDescriptor = nullptr;

	anonymous_pipe_create(&sub_to_main[1], &sub_to_main[0],
					&security_attributes, (DWORD)input.size,
					0, 0);

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	if (!SetHandleInformation(sub_to_main[1], HANDLE_FLAG_INHERIT, 0))
	{
		fprintf(stderr, "SetHandleInformation failed\n");
		exit(1);
	}

	anonymous_pipe_create(&main_to_sub[1], &main_to_sub[0],
					&security_attributes, (DWORD)input.size,
					0, 0);

	// Ensure the write handle to the pipe for STDIN is not inherited.
	if (!SetHandleInformation(main_to_sub[0], HANDLE_FLAG_INHERIT, 0))
	{
		fprintf(stderr, "SetHandleInformation failed\n");
		exit(1);
	}

	HANDLE child = child_process_create(cmd_line, main_to_sub[1], sub_to_main[0]);

	if(!child)
	{
		*status = COMMAND_DOES_NOT_EXIST;
		CloseHandle(main_to_sub[0]);
		CloseHandle(main_to_sub[1]);
		CloseHandle(sub_to_main[0]);
		CloseHandle(sub_to_main[1]);
		return rdi_buffer_init();
	}

	pipe_write(main_to_sub[0], input);
	RDI_Buffer output = pipe_read_all(sub_to_main[1]);
	//RDI_Buffer output = pipe_rw(sub_to_main[1], main_to_sub[0], input);

	WaitForSingleObject(child, INFINITE);
	GetExitCodeProcess(child, (LPDWORD)status);
	CloseHandle(child);

	return output;
}

RDI_Buffer
rdi_subprocess_io_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[])
{
	size_t cmd_size = strlen(argv[0]) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, argv[0]);

	for(size_t i = 1;; i++)
	{
		const char* tmp = argv[i];

		if(tmp == nullptr)
		{
			break;
		}

		size_t tmp_size = strlen(tmp);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + tmp_size + 1);
		memcpy(cmd_line.data + old_size, tmp, tmp_size);
	}

	cmd_line.data[cmd_line.size - 1] = '\0';

	RDI_Buffer output = rdi_subprocess_io_s(status, input_buffer, cmd_line.data);

	rdi_buffer_free(cmd_line);

	return output;
}

RDI_Buffer
rdi_subprocess_io(int* status, RDI_Const_Buffer input, const char* command, ...)
{
	va_list ap;
	size_t n;

	va_start(ap, command);
	for (n = 1; va_arg(ap, char*); n++);
	va_end(ap);

	size_t cmd_size = strlen(command) + 1;

	RDI_Buffer cmd_line = rdi_buffer_new(cmd_size);
	strcpy_s(cmd_line.data, cmd_size, command);

	va_start(ap, command);
	for(size_t i = 1; i < n; i++)
	{
		char* tmp = va_arg(ap, char*);

		char* arg = handle_spaces(tmp);
		size_t arg_size = strlen(arg);
		size_t old_size = cmd_line.size;
		cmd_line.data[cmd_line.size - 1] = ' ';
		cmd_line = rdi_buffer_resize(cmd_line, cmd_line.size + arg_size + 1);
		memcpy(cmd_line.data + old_size, arg, arg_size);
		free(arg);
	}
	va_end(ap);

	cmd_line.data[cmd_line.size - 1] = '\0';

	RDI_Buffer output = rdi_subprocess_io_s(status, input, cmd_line.data);

	rdi_buffer_free(cmd_line);

	return output;
}

#include <string.h>
#include <stdlib.h>

bool
command_exists(const char* command)
{
	// /q to return exit status
	const char* where = "where /q ";
	char* param = (char*)malloc((strlen(command)+strlen(where)+2)*sizeof(char));
	
	strcpy(param,where);
	strcat(param,command);

	// 0 for success
	int ret = system(param);

	free(param);
	param = NULL;

	return !ret;
}
